﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleGame
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 0, j = 0, health = 10;
            string[,] field = CreateGameField();
            string[,] mines = CreateMines();
            ShowGameField(field, health);
            while (field[9, 9] != "[☺]")
            {
                MovePlayer(ref field, ref i, ref j);
                GetDamage(ref field, ref mines, ref health, ref i, ref j);
                ShowGameField(field, health);
                if (!CheckHP(health))
                    break;
            }
            Console.ReadKey();
        }

        public static string[,] CreateGameField()
        {
            string[,] arr = new string[10, 10];
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    arr[i, j] = "[ ]";
                    if (i == 0 && j == 0)
                        arr[0, 0] = "[☺]";
                }
            }
            return arr;
        }

        public static string[,] CreateMines()
        {
            Random rnd = new Random();
            string[,] mines = new string[10, 10];
            int minposi = 0, minposj = 0;
            for (int i = 0; i < 10; i++)
            {
                minposi = rnd.Next(10);
                minposj = rnd.Next(10);
                while ((minposi == 0 && minposj == 0) || (minposi == 9 && minposj == 9) || (minposi == 0 && minposj == 1) || (minposi == 1 && minposj == 0))
                {
                    minposi = rnd.Next(10);
                    minposj = rnd.Next(10);
                }
                if (mines[minposi, minposj] != "O")
                    mines[minposi, minposj] = "O";
            }
            return mines;
        }

        public static void MovePlayer(ref string[,] arr, ref int i, ref int j)
        {
            switch (Console.ReadKey().Key.ToString())
            {
                case "UpArrow": try { arr[i - 1, j] = "[☺]"; if (arr[i, j] != "[O]") arr[i, j] = "[ ]"; i -= 1; } catch { }; break;
                case "RightArrow": try { arr[i, j + 1] = "[☺]"; if (arr[i, j] != "[O]") arr[i, j] = "[ ]"; j += 1; } catch { }; break;
                case "DownArrow": try { arr[i + 1, j] = "[☺]"; if (arr[i, j] != "[O]") arr[i, j] = "[ ]"; i += 1; } catch { }; break;
                case "LeftArrow": try { arr[i, j - 1] = "[☺]"; if (arr[i, j] != "[O]") arr[i, j] = "[ ]"; j -= 1; } catch { }; break;
            }
        }

        public static void GetDamage(ref string[,] arr, ref string[,] mines, ref int health, ref int i, ref int j)
        {
            Random rnd = new Random();
            if (mines[i, j] == "O")
            {
                health -= rnd.Next(1, 11);
                arr[i, j] = "[O]";
                mines[i, j] = "";
            }
        }

        public static void ShowGameField(string[,] arr, int health)
        {
            Console.Clear();
            Console.WriteLine($"HP:{health}");
            for (int ii = 0; ii < 10; ii++)
            {
                for (int jj = 0; jj < 10; jj++)
                {
                    Console.Write(arr[ii, jj]);
                }
                Console.WriteLine();
            }
        }

        public static bool CheckHP(int health)
        {
            if (health <= 0)
            {
                Console.WriteLine("Game Over!");
                return false;
            }
            return true;
        }

    }
}
